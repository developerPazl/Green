﻿using Green.Common.Exceptions.Abstraction;
using Green.Common.Services.Abstraction;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Green.API.Filter
{
    public class OnExceptionFilter : ExceptionFilterAttribute
    {
        public readonly ILogger logger;

        public OnExceptionFilter(ILogger logger)
        {
            this.logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            Exception exception = context.Exception;
            logger.LogException(exception); ;
        }
    }
}
