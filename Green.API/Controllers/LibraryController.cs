﻿using Green.API.Controllers.Abstraction;
using Green.Business.Services.Abstraction;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Green.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LibraryController : AuthorizeController
    {
        private readonly ILibraryService _library;

        public LibraryController(ILibraryService library)
        {
            _library = library;
        }
        [HttpGet("GetAllPackages")]
        public IActionResult GetAllPackages()
        {
            return _actionResult.GenerateActionResult(_library.GetAllPackage());
        }
        [HttpGet("GetAllDiscounts")]
        public IActionResult GetAllDiscounts()
        {
            return _actionResult.GenerateActionResult(_library.GetAllDiscount());
        }
    }
}
