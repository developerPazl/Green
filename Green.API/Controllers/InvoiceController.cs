﻿using Green.API.Controllers.Abstraction;
using Green.API.Dto.InComing;
using Green.Business.Model;
using Green.Business.Model.DTO;
using Green.Business.Services.Abstraction;
using Green.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Green.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : AuthorizeController
    {
        private readonly IInvoiceService _invoice;

        public InvoiceController(IInvoiceService invoice)
        {
            _invoice = invoice;
        }

        [HttpPost("NewInvoice")]
        public IActionResult CreateInvoice([FromBody]NewInvoiceDto invoice)
        {
            return _actionResult.GenerateActionResult(_invoice.CreateInvoice(invoice.Map<Invoice>()));
        }
        [HttpPost("RateInvoice")]
        public IActionResult CreateInvoice([FromQuery] int invoiceId, int starCount)
        {
            return _actionResult.GenerateActionResult(_invoice.RateInvoice(invoiceId, starCount));
        }
        [HttpPost("AddImage")]
        public IActionResult AddImage([FromQuery] int invoiceId, List<string> images)
        {
            return _actionResult.GenerateActionResult(_invoice.AddImage(invoiceId, images));
        }

    }
}
