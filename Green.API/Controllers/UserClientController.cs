﻿using Green.API.Controllers.Abstraction;
using Green.Business.Services.Abstraction;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Green.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserClientController : AuthorizeController
    {
        private readonly IUserClientService _userClientService;

        public UserClientController(IUserClientService userClientService)
        {
            _userClientService = userClientService;
        }

        [HttpGet("GetUser")]
        public IActionResult GetUser([FromHeader] string Authorization)
        {
            return _actionResult.GenerateActionResult(_userClientService.GetUser(Authorization));
        }
    }
}
