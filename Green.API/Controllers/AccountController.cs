﻿using Green.API.Controllers.Abstraction;
using Green.API.Dto.InComing;
using Green.Business.Model;
using Green.Business.Services.Abstraction;
using Green.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Green.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : BaseController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpPost("Login")]
        public IActionResult Login([FromBody]UserCredantialsDto user)
        {
            return _actionResult.GenerateActionResult(_accountService.SignIn(user.Map<UserCredentials>()));
        }
        [HttpPost("Register")]
        public IActionResult Register([FromBody]UserClientDto user)
        {
            return _actionResult.GenerateActionResult(_accountService.SignUp(user.Map<UserClient>()));
        }
    }
}
