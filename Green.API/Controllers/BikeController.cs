﻿using Green.API.Controllers.Abstraction;
using Green.Business.Services.Abstraction;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Green.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BikeController : AuthorizeController
    {
        public readonly IBikeService _bikeService;

        public BikeController(IBikeService bikeService)
        {
            _bikeService = bikeService;
        }
        [HttpGet("GetInfo")]
        public IActionResult GetInfo(string BikeId)
        {
            return _actionResult.GenerateActionResult(_bikeService.GetInfo(BikeId));
        }
        [HttpGet("UnLock")]
        public IActionResult UnLockBike(string BikeId)
        {
            return _actionResult.GenerateActionResult(_bikeService.UnLock(BikeId));
        }
        [HttpGet("TurnLights")]
        public IActionResult TurnOnOrOffLights(string BikeId, int lightNumber, bool turnType)
        {
            return _actionResult.GenerateActionResult(_bikeService.TurnLights(BikeId, lightNumber, turnType));
        }
    }
}
