﻿using Green.Common.Config.Absraction;
using Green.Common.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Green.API.Config
{
    public class Configration : IDependencyInject
    {
        public IDependencyInject Register(IServiceCollection services = null, IConfiguration configuration = null)
        {
            GMapperConfiguration.Instance.AddProfile(new MapProfile());
            var businessInject = new Business.Config.DependencyInject().Register(services);
            var commonInject = new Common.Config.DependencyInject().Register(services, configuration);
            var domainInject = new Domain.Config.DependencyInject().Register(services, configuration);
            var servicesMqttInject = new Services.MQTT.Config.DependencyInject().Register(services, configuration);
            return this;
        }
    }
}
