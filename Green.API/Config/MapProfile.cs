﻿using AutoMapper;
using Green.API.Dto.InComing;
using Green.Business.Model;

namespace Green.API.Config
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            this.CreateMap<UserClientDto, UserClient>();
            this.CreateMap<NewInvoiceDto, Invoice>();
            this.CreateMap<UserCredantialsDto, UserClient>();
        }
    }
}
