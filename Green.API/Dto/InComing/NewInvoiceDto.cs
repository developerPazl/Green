﻿namespace Green.API.Dto.InComing
{
    public class NewInvoiceDto
    {
        public int UserClientId { get; set; }
        public string BikeAddress { get; set; }
        public int PackageId { get; set; }
        public int? DiscountId { get; set; }
        public int? Distance { get; set; }
        public int? Time { get; set; }
    }
}
