﻿namespace Green.API.Dto.OutGoing
{
    public class UserTokenDto
    {
        public string Token { get; set; }
    }
}
