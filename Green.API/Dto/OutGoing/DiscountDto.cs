﻿namespace Green.API.Dto.OutGoing
{
    public class DiscountDto
    {
        public string Code { get; set; }
        public bool IsActive { get; set; }
        public int DiscountPercent { get; set; }
        public decimal DiscountAmount { get; set; }
        public int Type { get; set; }
    }
}
