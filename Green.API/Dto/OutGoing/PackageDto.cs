﻿namespace Green.API.Dto.OutGoing
{
    public class PackageDto
    {
        public string Header { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int PerDistance { get; set; }
        public int PerTime { get; set; }
        public int Type { get; set; }
    }
}
