﻿namespace Green.API.Dto.OutGoing
{
    public class BikeDto
    {
        public string BikeUID { get; set; }
        public string Plate { get; set; }
        public int Charge { get; set; }
        public decimal Fee { get; set; }
        public bool Lock { get; set; }
    }
}
