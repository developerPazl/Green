﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Services.MQTT.Model
{
    public class MQTTResponseModel
    {
        public string id { get; set; }
        public bool result { get; set; }
        public int reason { get; set; }
    }
}
