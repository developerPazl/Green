﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Services.MQTT.Model
{
    public class SyncModel
    {
        public class GPS
        {
            public float lng { get; set; }
            public float lat { get; set; }
            public float alt { get; set; }
        }
        public class Lights
        {
            public bool front { get; set; }
            public bool rear { get; set; }
        }
        public double battery { get; set; }
        public GPS gps { get; set; }
        public Lights lights { get; set; }
        public int locked { get; set; }
        public int speed { get; set; }
    }
}
