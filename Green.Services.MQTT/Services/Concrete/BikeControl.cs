﻿using Green.Services.MQTT.Services.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using M2MQTT = uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt;
using Microsoft.Extensions.Options;
using AppSettingMQTT = Green.Common.Model.System.MQTT;
using Green.Common.Model.System;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading;
using Green.Services.MQTT.Model;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace Green.Services.MQTT.Services.Concrete
{
    public class BikeControl : BaseServices, IBikeControl
    {
        private SyncModel ResultSyncModel;
        private MQTTResponseModel ResultResponse;
        private FlagModel Flag = new FlagModel();
        private readonly AppSettingMQTT mqtt;
        public BikeControl(IOptions<AppSettings> appSettings)
        {
            mqtt = appSettings.Value.MQTT;
        }

        public async Task<bool> LockBike(string BikeId, bool locking)
        {
            if (BikeId == null)
            {
                return false;
            }
            var client = GenerateMQTTWithToken(mqtt.Host, mqtt.Port, mqtt.ClientId, mqtt.Username);
            if (client == null)
            {
                return false;
            }

            string messageId = GenerateMessageId();

            var data = new
            {
                id = messageId,
                command = "setLock",
                args = new 
                { 
                    @lock = locking
                }
            };
            var dataJson = JsonConvert.SerializeObject(data);
            client.Subscribe(new string[] { BikeId + "/TX" }, new byte[] { 0 });
            client.MqttMsgPublishReceived += ClientResponse;
            client.Publish(BikeId + "/RX", Encoding.UTF8.GetBytes(dataJson));
            await AwaitResponse();
            if(ResultResponse !=null && ResultResponse.id == messageId && ResultResponse.result)
            {
                return true;
            }
            return false;
        }
        private async Task AwaitResponse()
        {
            int timeOut = Flag.TimeOut;
            while (!Flag.Flag)
            {
                await Task.Delay(50);
                timeOut -= 50;
                if(timeOut<0)
                {
                    return;
                }
            }
        }

        private void ClientResponse(object sender, M2MQTT.Messages.MqttMsgPublishEventArgs e)
        {
            string message = Encoding.UTF8.GetString(e.Message);
            ResultResponse = JsonConvert.DeserializeObject<MQTTResponseModel>(message);
            Flag.Flag = true;
        }

        public async Task<bool> TurnLights(string BikeId, int index, bool turnType)
        {
            if (BikeId == null)
            {
                return false;
            }
            var client = GenerateMQTTWithToken(mqtt.Host, mqtt.Port, mqtt.ClientId, mqtt.Username);
            if (client == null)
            {
                return false;
            }

            string messageId = GenerateMessageId();

            dynamic data;

            if (index == 0)
            {
                data = new
                {
                    id = messageId,
                    command = "setLamp",
                    args = new
                    {
                        front = turnType,
                    }
                };
            }
            else if (index == 1)
            {
                data = new
                {
                    id = messageId,
                    command = "setLamp",
                    args = new
                    {
                        rear = turnType
                    }
                };
            }
            else
                return false;

            
            var dataJson = JsonConvert.SerializeObject(data);
            client.Subscribe(new string[] { BikeId + "/TX" }, new byte[] { 0 });
            client.MqttMsgPublishReceived += ClientResponse;
            client.Publish(BikeId + "/RX", Encoding.UTF8.GetBytes(dataJson));
            await AwaitResponse();
            if (ResultResponse != null && ResultResponse.id == messageId && ResultResponse.result)
            {
                return true;
            }
            return false;
        }

        public async Task<object> SyncBike(string BikeId)
        {
            if (BikeId == null)
            {
                return null;
            }
            var client = GenerateMQTTWithToken(mqtt.Host, mqtt.Port, mqtt.ClientId, mqtt.Username);
            if (client == null)
            {
                return null;
            }

            string messageId = GenerateMessageId();

            var data = new
            {
                id = messageId,
                command = "sync"
            };
            var dataJson = JsonConvert.SerializeObject(data);
            client.Subscribe(new string[] { BikeId + "/TX" }, new byte[] { 0 });
            client.MqttMsgPublishReceived += SyncClientResponse;
            client.Publish(BikeId + "/RX", Encoding.UTF8.GetBytes(dataJson));
            await AwaitResponse();
            if (ResultSyncModel != null)
            {
                return ResultSyncModel;
            }
            return null;
        }

        private void SyncClientResponse(object sender, MqttMsgPublishEventArgs e)
        {
            string message = Encoding.UTF8.GetString(e.Message);
            ResultSyncModel = JsonConvert.DeserializeObject<SyncModel>(message);
            Flag.Flag = true;
        }
    }

}
