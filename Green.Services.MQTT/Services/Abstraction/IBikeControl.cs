﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Services.MQTT.Services.Abstraction
{
    public interface IBikeControl
    {
        Task<bool> LockBike(string BikeAdress, bool locking);
        Task<bool> TurnLights(string BikeId, int index, bool turnType);
        Task<object> SyncBike(string BikeId);
    }
}
