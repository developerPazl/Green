﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;

namespace Green.Services.MQTT.Services.Abstraction
{
    public abstract class BaseServices
    {
        public string MqttToken { get; private set; }
        public void GenerateToken(string username, string clientid)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            //you should store the secret key in the appSettings like appSettings.Secret
            //var key = Encoding.ASCII.GetBytes(appSettings.Secret);
            var key = Encoding.ASCII.GetBytes("8Md??j5Dm6M5NMSD$oia6!Rd@7!a8gHdD7$6APLC");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("username", username), new Claim("clientid", clientid) }),
                Expires = DateTime.UtcNow.AddHours(8),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            MqttToken = tokenHandler.WriteToken(token);
        }
        public MqttClient ConnectMQTT(string broker, int port, string clientId, string username, string password)
        {
            MqttClient client = new MqttClient(broker, port, true, MqttSslProtocols.TLSv1_2, null, null);
            client.Connect(clientId, username, password);
            if (client.IsConnected)
            {
                return client;
            }
            else
            {
                return null;
            }
        }
        public MqttClient GenerateMQTTWithToken(string broker, int port, string clientId, string username)
        {
            GenerateToken(username, clientId);
            var client = ConnectMQTT(broker, port, clientId, username, MqttToken);
            return client;
        }

        public string GenerateMessageId()
        {
            return Guid.NewGuid().ToString().ToUpper();
        }
    }
}
