﻿using Green.Common.Config.Absraction;
using Green.Services.MQTT.Services.Abstraction;
using Green.Services.MQTT.Services.Concrete;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Services.MQTT.Config
{
    public class DependencyInject : IDependencyInject
    {
        public IDependencyInject Register(IServiceCollection services = null, IConfiguration configuration = null)
        {
            services.AddTransient<IBikeControl, BikeControl>();
            return this;
        }
    }
}
