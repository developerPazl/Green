﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Model
{
    public class Invoice
    {
        [IgnoreDataMember]
        public string InvoiceNumber { get; set; }
        public int UserClientId { get; set; }
        public string BikeAddress { get; set; }
        [IgnoreDataMember]
        public int BikeId { get; set; }
        public int PackageId { get; set; }
        public int? DiscountId { get; set; }
        [IgnoreDataMember]
        public decimal? TotalAmount { get; set; }
        public int? Distance { get; set; }
        public int? Time { get; set; }
        public int? Rate { get; set; }

        [IgnoreDataMember]
        public UserClient UserClient { get; set; }
        [IgnoreDataMember]
        public Bike Bike { get; set; }
        [IgnoreDataMember]
        public Package Package { get; set; }
        [IgnoreDataMember]
        public Discount Discount { get; set; }
    }
}
