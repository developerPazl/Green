﻿using Green.Business.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Model
{
    public class ServiceResult<T>
    {
        public ServiceResultType ResultTypeId { get; private set; }

        public string MessageCode { get; set; }


        public T Value { get; private set; }

        public static ServiceResult<T> Ok(T value, string messageCode = null)
        {
            return new ServiceResult<T>()
            {
                ResultTypeId = ServiceResultType.Ok,
                Value = value,
                MessageCode = messageCode
            };
        }

        public static ServiceResult<T> Ok(string messageCode = null)
        {
            return new ServiceResult<T>()
            {
                ResultTypeId = ServiceResultType.Ok,
                MessageCode = messageCode
            };
        }

        public static ServiceResult<T> BadRequest(T value, string messageCode = null)
        {
            return new ServiceResult<T>()
            {
                ResultTypeId = ServiceResultType.BadRequest,
                Value = value,
                MessageCode = messageCode
            };
        }

        public static ServiceResult<T> BadRequest(string messageCode)
        {
            return new ServiceResult<T>()
            {
                ResultTypeId = ServiceResultType.BadRequest,
                MessageCode = messageCode
            };
        }

        public static ServiceResult<T> Created(T value, string messageCode = null)
        {
            return new ServiceResult<T>()
            {
                ResultTypeId = ServiceResultType.Created,
                Value = value,
                MessageCode = messageCode
            };
        }

        public static ServiceResult<T> NotFound(string messageCode = null)
        {
            return new ServiceResult<T>()
            {
                ResultTypeId = ServiceResultType.NotFound,
                MessageCode = messageCode
            };
        }

        public static ServiceResult<T> Unauthorized(string messageCode = null)
        {
            return new ServiceResult<T>()
            {
                ResultTypeId = ServiceResultType.Unauthorized,
                MessageCode = messageCode
            };
        }

        private static ServiceResult<T> MakeBySelf(T value, string messageCode = null)
        {
            if (value == null)
                return NotFound(messageCode);
            else
                return Created(value, messageCode);
        }
    }

}
