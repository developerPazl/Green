﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Model
{
    public class UserTokenInfo
    {
        public string Token { get; set; }
    }
}
