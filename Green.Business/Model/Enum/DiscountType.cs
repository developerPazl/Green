﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Model.Enum
{
    public enum DiscountType
    {
        Amount = 1,
        Percent = 2
    }
}
