﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Model.Enum
{
    public enum ServiceResultType
    {
        Ok = 200,
        BadRequest = 400,
        Created = 201,
        NotFound = 404,
        Unauthorized = 401
    }

}
