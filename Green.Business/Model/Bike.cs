﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Model
{
    public class Bike
    {
        public string BikeUID { get; set; }
        public string Plate { get; set; }
        public int Charge { get; set; }
        public decimal Fee { get; set; }
        public bool Lock { get; set; }
    }
}
