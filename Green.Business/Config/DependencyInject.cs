﻿using Green.Business.Services.Abstraction;
using Green.Business.Services.Concrete;
using Green.Common.Config.Absraction;
using Green.Common.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Config
{
    public class DependencyInject : IDependencyInject
    {

        public IDependencyInject Register(IServiceCollection services = null, IConfiguration configuration = null)
        {
            GMapperConfiguration.Instance.AddProfile(new MapProfile());
            services.AddScoped<ITokenService, TokenService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IBikeService, BikeService>();
            services.AddScoped<IInvoiceService, InvoiceService>();
            services.AddScoped<ILibraryService, LibraryService>();
            services.AddScoped<IUserClientService, UserClientService>();
            return this;
        }
    }
}
