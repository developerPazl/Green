﻿using Green.Business.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Config
{
    public class ActionResult
    {
        public IActionResult GenerateActionResult<T>(ServiceResult<T> serviceResult)
        {
            HttpResponseMessage responseMessage;
            if (serviceResult == null)
                responseMessage = CreateHttpResponseMessage(200, null);
            else
                responseMessage = CreateHttpResponseMessage((int)serviceResult.ResultTypeId, serviceResult);

            return new ObjectResult(responseMessage)
            {
                Value = serviceResult,
            };
        }

        private HttpResponseMessage CreateHttpResponseMessage(int httpStatusCode, object content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.StatusCode = (HttpStatusCode)httpStatusCode;

            JsonMediaTypeFormatter camelCasingFormatter = new JsonMediaTypeFormatter();
            camelCasingFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            response.Content = new ObjectContent<object>(content, camelCasingFormatter, "application/json");

            return response;
        }
    }
}
