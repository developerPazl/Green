﻿using AutoMapper;
using Green.Business.Model;
using Green.Business.Model.DTO;
using Green.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Config
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            this.CreateMap<Bike, BikeDM>().ReverseMap();
            this.CreateMap<UserClient, UserClientDM>().ReverseMap();
            this.CreateMap<Package, PackageDM>().ReverseMap();
            this.CreateMap<Discount, DiscountDM>().ReverseMap();
            this.CreateMap<Invoice, InvoiceDM>().ReverseMap();
        }
    }
}
