﻿using Green.Business.Model;
using Green.Business.Services.Abstraction;
using Green.Domain.Entity;
using Green.Domain.Unit.Abstraction;
using Green.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Concrete
{
    public class UserClientService : IUserClientService
    {
        private readonly ITokenService _tokenService;
        private readonly IUnitOfWork _db;

        public UserClientService(
            ITokenService tokenService,
            IUnitOfWork db)
        {
            _tokenService = tokenService;
            _db = db;
        }

        public ServiceResult<UserClient> GetUser(string token)
        {
            int userClientId = _tokenService.GetSubjectFromToken(token);
            if (userClientId == -1)
                return ServiceResult<UserClient>.BadRequest("Not Authorised");
            var userClient = _db.Repository<UserClientDM>()
                                .Get(x => x.Id == userClientId);
            if (userClient == null)
                return ServiceResult<UserClient>.BadRequest("User cannot found");
            return ServiceResult<UserClient>.Ok(userClient.Map<UserClient>());
        }
    }
}
