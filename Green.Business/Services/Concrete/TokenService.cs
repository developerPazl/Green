﻿using Green.Business.Services.Abstraction;
using Green.Common.Model.System;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Concrete
{
    public class TokenService : ITokenService
    {
        private IOptions<AppSettings> _appSettings;

        public TokenService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings;
        }

        public string GenerateToken(int Id)
        {
            string JWTTokenSymmetricKey = _appSettings.Value.JWTTokenBearer;
            var creds = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JWTTokenSymmetricKey)),
                                               SecurityAlgorithms.HmacSha256Signature);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] { new Claim("UserClientId", Id.ToString())}),
                Expires = DateTime.Now.AddHours(3),
                SigningCredentials = creds
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var sessionToken = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(sessionToken);
        }

        public string GetHash(string password)
        {
            using (var sha256 = SHA256.Create())
            {
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(password));
                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }

        public int GetSubjectFromToken(string Token)
        {
            int subject = -1;
            if (string.IsNullOrEmpty(Token?.Trim()))
                return subject;
            string cleanToken = Token.Replace("Bearer ", "");

            var jwtToken = new JwtSecurityToken(cleanToken);
            var userClientId = jwtToken.Claims.FirstOrDefault(x => x.Type == "UserClientId")?.Value;

            bool tryParseResult = int.TryParse(userClientId, out subject);
            if (!tryParseResult)
                return subject;
            return subject;
        }
    }
}
