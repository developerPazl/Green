﻿using Green.Business.Model;
using Green.Business.Model.DTO;
using Green.Business.Services.Abstraction;
using Green.Domain.Entity;
using Green.Domain.Unit.Abstraction;
using Green.Extensions;
using Green.Services.MQTT.Services.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Concrete
{
    public class BikeService : IBikeService
    {
        public readonly IBikeControl _bikeControl;
        public readonly IUnitOfWork _db;

        public BikeService(IBikeControl bikeControl, IUnitOfWork unitOfWork)
        {
            _db = unitOfWork;
            _bikeControl = bikeControl;
        }

        public ServiceResult<object> GetInfo(string BikeId)
        {
            //var bike = _db.GreenDbContext.GetRepository<BikeDM>()
            //                             .Get(x => x.BikeUID == BikeId);
            var bike = _bikeControl.SyncBike(BikeId).Result;
            if(bike == null)
            {
                return ServiceResult<object>.BadRequest(BikeId + " does not exist");
            }
            return ServiceResult<object>.Ok(bike);
        }

        public ServiceResult<NullResponseDto> TurnLights(string BikeId, int lightIndex, bool turnType)
        {
            var result = _bikeControl.TurnLights(BikeId, lightIndex, turnType).Result;
            if(result)
                return ServiceResult<NullResponseDto>.Ok();
            return ServiceResult<NullResponseDto>.BadRequest("Bike not respond");
        }

        public ServiceResult<NullResponseDto> UnLock(string BikeId)
        {
            var result = _bikeControl.LockBike(BikeId, false).Result;
            if(result)
            {
                return ServiceResult<NullResponseDto>.Ok("Bike unlocked");
            }
            else
            {
                return ServiceResult<NullResponseDto>.BadRequest("Bike not responding");
            }
        }
    }
}
