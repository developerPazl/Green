﻿using Green.Business.Model;
using Green.Business.Model.DTO;
using Green.Business.Model.Enum;
using Green.Business.Services.Abstraction;
using Green.Domain.Entity;
using Green.Domain.Unit.Abstraction;
using Green.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Concrete
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IUnitOfWork _db;

        public InvoiceService(IUnitOfWork db)
        {
            _db = db;
        }

        public ServiceResult<NullResponseDto> AddImage(int invoiceId, List<string> images)
        {
            if(images == null)
                return ServiceResult<NullResponseDto>.BadRequest("Images is empty");

            bool hasInvoice = _db.Repository<InvoiceDM>()
                                 .Any(x => x.Id == invoiceId);
            if (!hasInvoice)
                return ServiceResult<NullResponseDto>.BadRequest("There is not invoice in the system like this");
            int i = 0;
            foreach(var image in images)
            {
                if (!image.IsValidBase64())
                    continue;
                _db.Repository<InvoiceImageDM>()
                   .Add(new InvoiceImageDM
                   {
                       InvoiceId = invoiceId,
                       Image = image
                   });
                i++;
            }
            if (i == 0)
                return ServiceResult<NullResponseDto>.BadRequest("Cannot saved image");

            _db.SaveChanges();
            return ServiceResult<NullResponseDto>.Ok("ok");
        }

        public ServiceResult<NullResponseDto> CreateInvoice(Invoice invoice)
        {
            invoice.UserClient = null;
            invoice.Bike = null;
            invoice.Discount = null;
            invoice.Package = null;

            decimal totalAmount = 0;

            bool hasUser = _db.Repository<UserClientDM>()
                              .Any(x => x.Id == invoice.UserClientId);
            if (!hasUser)
                return ServiceResult<NullResponseDto>.BadRequest("There is not user in the system like this");

            if(invoice.BikeAddress == null)
                return ServiceResult<NullResponseDto>.BadRequest("There is not bike in the system like this");

            var hasBike = _db.Repository<BikeDM>()
                              .Get(x => x.BikeUID == invoice.BikeAddress);
            if(hasBike == null)
                return ServiceResult<NullResponseDto>.BadRequest("There is not bike in the system like this");

            var hasPackage = _db.Repository<PackageDM>()
                                .Get(x => x.Id == invoice.PackageId);
            if (hasPackage == null)
                return ServiceResult<NullResponseDto>.BadRequest("There is not package in the system like this");

            if(hasPackage.Type == (int)PackageType.PerDistance)
            {
                if(!invoice.Distance.HasValue ||  invoice.Distance<=0)
                    return ServiceResult<NullResponseDto>.BadRequest("Invoice cannot created. Because distance is invaild");

                totalAmount = (Convert.ToDecimal(invoice.Distance.Value) / Convert.ToDecimal(hasPackage.PerDistance)) * Convert.ToDecimal(hasPackage.Price);
            }
            else if(hasPackage.Type == (int)PackageType.PerTime)
            {
                if (!invoice.Time.HasValue || invoice.Time <= 0)
                    return ServiceResult<NullResponseDto>.BadRequest("Invoice cannot created. Because time is invaild");

                totalAmount = (Convert.ToDecimal(invoice.Time.Value) / Convert.ToDecimal(hasPackage.PerTime)) * Convert.ToDecimal(hasPackage.Price);
            }


            if(invoice.DiscountId != null)
            {
                var hasDiscount = _db.Repository<DiscountDM>()
                                     .Get(x => x.Id == invoice.DiscountId);
                if (hasDiscount ==null)
                    return ServiceResult<NullResponseDto>.BadRequest("There is not discount in the system like this");

                if(hasDiscount.IsActive)
                {
                    if (hasDiscount.Type == (int)DiscountType.Amount)
                        totalAmount = totalAmount - Convert.ToDecimal(hasDiscount.DiscountAmount);
                    else if (hasDiscount.Type == (int)DiscountType.Percent)
                        totalAmount = totalAmount - ((totalAmount * Convert.ToDecimal(hasDiscount.DiscountPercent)) / 100);
                }
            }

            if (totalAmount < 0)
                totalAmount = 0;

            var invoiceDM = invoice.Map<InvoiceDM>();
            invoiceDM.TotalAmount = totalAmount;

            string invoiceNumber = string.Empty;
            invoiceNumber += "UCI";
            invoiceNumber += invoice.UserClientId.ToString();
            invoiceNumber += "-";
            invoiceNumber += "IPI";
            invoiceNumber += invoice.PackageId.ToString();
            invoiceNumber += "-";
            invoiceNumber += "IBI";
            invoiceNumber += invoice.BikeId.ToString();
            invoiceNumber += "-";
            invoiceNumber += DateTime.Now.ToString("ddMMyyyy");
            invoiceNumber += "-";
            invoiceNumber += Guid.NewGuid().ToString().ToUpper();

            invoiceDM.InvoiceNumber = invoiceNumber;

            _db.Repository<InvoiceDM>()
               .Add(invoiceDM);
            _db.SaveChanges();

            return ServiceResult<NullResponseDto>.Ok("Ok");
        }

        public ServiceResult<NullResponseDto> RateInvoice(int invoiceId, int starCount)
        {
            bool hasInvoice = _db.Repository<InvoiceDM>()
                                 .Any(x => x.Id == invoiceId);
            if (!hasInvoice)
                return ServiceResult<NullResponseDto>.BadRequest("There is not invoice in the system like this");

            if(starCount < 0 || starCount > 5)
            {
                return ServiceResult<NullResponseDto>.BadRequest("Invalid rating score");
            }

            _db.Repository<InvoiceDM>()
               .Update(new InvoiceDM()
               {
                   Id = invoiceId,
                   Rate = starCount
               }, x => x.Rate);
            _db.SaveChanges();

            return ServiceResult<NullResponseDto>.Ok("ok");
        }
    }
}
