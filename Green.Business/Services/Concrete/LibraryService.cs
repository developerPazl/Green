﻿using Green.Business.Model;
using Green.Business.Services.Abstraction;
using Green.Domain.Entity;
using Green.Domain.Unit.Abstraction;
using Green.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Concrete
{
    public class LibraryService : ILibraryService
    {
        private readonly IUnitOfWork _db;

        public LibraryService(IUnitOfWork db)
        {
            _db = db;
        }

        public ServiceResult<List<Discount>> GetAllDiscount()
        {
            var discountsDM = _db.Repository<DiscountDM>()
                                 .Find(x => true);
            var discounts = discountsDM.Map<List<Discount>>();
            return ServiceResult<List<Discount>>.Ok(discounts);
        }

        public ServiceResult<List<Package>> GetAllPackage()
        {
            var packagesDM = _db.Repository<PackageDM>()
                                .Find(x => true);
            var packages = packagesDM.Map<List<Package>>();
            return ServiceResult<List<Package>>.Ok(packages);
        }
    }
}
