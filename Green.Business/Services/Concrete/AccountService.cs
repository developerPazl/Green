﻿using Green.Business.Model;
using Green.Business.Model.DTO;
using Green.Business.Services.Abstraction;
using Green.Domain.Entity;
using Green.Domain.Unit.Abstraction;
using Green.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Concrete
{
    public class AccountService : IAccountService
    {
        private readonly ITokenService _tokenService;
        private readonly IUnitOfWork _db;

        public AccountService(ITokenService tokenService, IUnitOfWork db)
        {
            _db = db;
            _tokenService = tokenService;
        }

        public ServiceResult<UserTokenInfo> SignIn(UserCredentials user)
        {
            user.UserName = user.UserName.ToLower();
            user.Password = _tokenService.GetHash(user.Password);

            var userDM = _db.Repository<UserClientDM>()
                            .Get(x => x.Username == user.UserName ||
                                    x.EMail == user.UserName ||
                                    x.PhoneNumber == user.UserName);
            
            if (userDM != null && user.Password == userDM.Password)
            {
                return ServiceResult<UserTokenInfo>.Ok((new UserTokenInfo()
                {
                    Token = _tokenService.GenerateToken(userDM.Id)
                }));
            }
            return ServiceResult<UserTokenInfo>.BadRequest("Username or password is Invalid");
        }

        public ServiceResult<NullResponseDto> SignUp(UserClient user)
        {
            if(!user.EMail.IsValidEmail())
            {
                return ServiceResult<NullResponseDto>.BadRequest("Email is Invalid.");
            }
            if(!user.PhoneNumber.IsValidPhoneNumber())
            {
                return ServiceResult<NullResponseDto>.BadRequest("Phone number is invalid");
            }
            if (user.Name == null)
            {
                return ServiceResult<NullResponseDto>.BadRequest("Name is Invalid.");
            }
            if (user.SurName == null)
            {
                return ServiceResult<NullResponseDto>.BadRequest("Surname is Invalid.");
            }
            if (user.UserName == null || user.UserName.Length < 6)
            {
                return ServiceResult<NullResponseDto>.BadRequest("Username is invalid or less than 6 symbols.");
            }
            if(!user.Password.IsValidPassword())
            {
                return ServiceResult<NullResponseDto>.BadRequest("Password is invalid. Password required consist is upper case, symbols (, . /) and number.");
            }
            var hasAnyUserEmail = _db.Repository<UserClientDM>()
                                     .Any(x => x.EMail == user.EMail);
            var hasAnyUserPhoneNumber = _db.Repository<UserClientDM>()
                                           .Any(x => x.PhoneNumber == user.PhoneNumber);
            var hasAnyUserUsername = _db.Repository<UserClientDM>()
                                        .Any(x => x.Username == user.UserName);
            if (hasAnyUserEmail)
            {
                return ServiceResult<NullResponseDto>.BadRequest("Email was already registered");
            }
            if (hasAnyUserPhoneNumber)
            {
                return ServiceResult<NullResponseDto>.BadRequest("Phone number was already registered");
            }
            if (hasAnyUserUsername)
            {
                return ServiceResult<NullResponseDto>.BadRequest("Username was already registered");
            }
            var userDM = user.Map<UserClientDM>();
            userDM.Password = _tokenService.GetHash(userDM.Password);
            userDM.Username = userDM.Username.ToLower();
            userDM.EMail = userDM.EMail.ToLower();
            _db.Repository<UserClientDM>().Add(userDM);
            _db.SaveChanges();
            return ServiceResult<NullResponseDto>.Ok("Created new User");
        }
    }
}
