﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Abstraction
{
    public interface ITokenService
    {
        string GenerateToken(int Id);
        int GetSubjectFromToken(string Token);
        string GetHash(string password);
    }
}
