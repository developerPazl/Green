﻿using Green.Business.Model;
using Green.Business.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Abstraction
{
    public interface IBikeService
    {
        ServiceResult<object> GetInfo(string BikeId);
        ServiceResult<NullResponseDto> UnLock(string BikeId);
        ServiceResult<NullResponseDto> TurnLights(string BikeId, int lightIndex, bool turnType);
    }
}
