﻿using Green.Business.Model;
using Green.Business.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Abstraction
{
    public interface IInvoiceService
    {
        ServiceResult<NullResponseDto> CreateInvoice(Invoice invoice);
        ServiceResult<NullResponseDto> RateInvoice(int InvoiceId, int starCount);
        ServiceResult<NullResponseDto> AddImage(int InvoiceId, List<string> images);
    }
}
