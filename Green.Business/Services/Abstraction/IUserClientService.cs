﻿using Green.Business.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Abstraction
{
    public interface IUserClientService
    {
        ServiceResult<UserClient> GetUser(string token);
    }
}
