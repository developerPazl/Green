﻿using Green.Business.Model;
using Green.Business.Model.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Business.Services.Abstraction
{
    public interface IAccountService
    {
        ServiceResult<UserTokenInfo> SignIn(UserCredentials user);
        ServiceResult<NullResponseDto> SignUp(UserClient user);
    }
}
