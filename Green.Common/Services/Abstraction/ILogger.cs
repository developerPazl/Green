﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Common.Services.Abstraction
{
    public interface ILogger
    {
        void LogException(Exception exception);
        void Log(bool withDatetime = true);
        void Log(string logMessage, bool withDatetime = true);
    }
}
