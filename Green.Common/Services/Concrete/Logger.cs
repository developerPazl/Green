﻿using Green.Common.Exceptions;
using Green.Common.Model.System;
using Green.Common.Services.Abstraction;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Common.Services.Concrete
{
    public class Logger : ILogger
    {
        public readonly IOptions<FileDirections> _fileDirections;

        public Logger(IOptions<FileDirections> fileDirections)
        {
            _fileDirections = fileDirections;
        }

        public void Log(bool withDatetime = true)
        {
            try
            {
                throw new LoggerException();
            }
            catch(Exception exception)
            {
                LogException(exception);
            }
        }

        public void Log(string logMessage, bool withDatetime = true)
        {
            LogToFile(logMessage, false, withDatetime);
        }

        public void LogException(Exception exception)
        {
            StringBuilder stringBuilder = new StringBuilder();
            while(exception.InnerException !=null)
            {
                stringBuilder.AppendLine(exception.Message);
                exception = exception.InnerException;
            }
            string log = stringBuilder.ToString();
            LogToFile(log, true, true);
        }

        private void LogToFile(string logMessage, bool isException = false, bool withDatetime = true)
        {
            StringBuilder stringBuilder = new StringBuilder();
            if(withDatetime)
            {
                stringBuilder.AppendLine(DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
            }
            stringBuilder.AppendLine(logMessage);
            stringBuilder.AppendLine();
            stringBuilder.AppendLine();
            string log = stringBuilder.ToString();

            string fileName = DateTime.Now.ToString("ddMMyyyy") + ".txt";

            if (!Directory.Exists(_fileDirections.Value.LogFileDirection))
                Directory.CreateDirectory(_fileDirections.Value.LogFileDirection);

            using (StreamWriter writer = File.AppendText(_fileDirections.Value.LogFileDirection + fileName))
                writer.WriteLine(log);

            if(isException)
            {
                if (!Directory.Exists(_fileDirections.Value.LogExceptionDirection))
                    Directory.CreateDirectory(_fileDirections.Value.LogExceptionDirection);

                using (StreamWriter writer = File.AppendText(_fileDirections.Value.LogExceptionDirection + fileName))
                    writer.WriteLine(log);
            }

        }
    }
}
