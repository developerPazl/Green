﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Common.Model.System
{
    public class MQTT
    {
        public string ClientId { get; set; }
        public string Username { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
    }
}
