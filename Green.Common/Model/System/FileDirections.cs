﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Common.Model.System
{
    public class FileDirections
    {
        public string LogFileDirection { get; set; }
        public string LogExceptionDirection { get; set; }
    }
}
