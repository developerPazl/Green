﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Common.Model.System
{
    public class AppSettings
    {
        public string JWTTokenBearer { get; set; }
        public MQTT MQTT { get; set; }
    }
}
