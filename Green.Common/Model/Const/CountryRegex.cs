﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Common.Model.Const
{
    public class CountryRegex
    {
        public const string TurkeyPattern = @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}";
    }
}
