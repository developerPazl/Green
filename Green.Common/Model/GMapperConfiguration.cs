﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Common.Model
{
    public class GMapperConfiguration
    {
        private static MapperConfigurationExpression _instance;
        public static MapperConfigurationExpression Instance 
        { 
            get
            {
                if (_instance == null)
                {
                    _instance = new MapperConfigurationExpression();
                }
                return _instance;
            } 
        }
        public static IMapper GetMapper
        {
            get
            {
                Mapper mapper = new Mapper(new MapperConfiguration(Instance));
                return mapper;
            }
        }
    }
}
