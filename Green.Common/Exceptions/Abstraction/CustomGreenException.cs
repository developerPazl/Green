﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Green.Common.Exceptions.Abstraction
{
    public abstract class CustomGreenException : Exception
    {
        protected CustomGreenException()
        {
        }

        protected CustomGreenException(string message) : base(message)
        {
        }

        protected CustomGreenException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        protected CustomGreenException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
