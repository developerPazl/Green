﻿using Green.Common.Exceptions.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Common.Exceptions
{
    public class LoggerException : CustomGreenException
    {
        public LoggerException() : base("Log File")
        {
        }
    }
}
