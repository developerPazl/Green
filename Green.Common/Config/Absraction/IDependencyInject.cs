﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Common.Config.Absraction
{
    public interface IDependencyInject
    {
        IDependencyInject Register(IServiceCollection services = null, IConfiguration configuration = null);
    }
}
