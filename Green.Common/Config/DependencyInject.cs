﻿using Green.Common.Config.Absraction;
using Green.Common.Model.System;
using Green.Common.Services.Abstraction;
using Green.Common.Services.Concrete;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Green.Common.Config
{
    public class DependencyInject : IDependencyInject
    {
        public IDependencyInject Register(IServiceCollection services=null, IConfiguration Configuration=null)
        {
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            services.Configure<FileDirections>(Configuration.GetSection("FileDirections"));

            services.AddScoped<ILogger, Logger>();

            return this;
        }
    }
}
