﻿using Green.Domain.DB.Context;
using Green.Domain.DB.Context.Abstraction;
using Green.Domain.DB.Repository.Abstraction;
using Green.Domain.Entity.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Unit.Abstraction
{
    public interface IUnitOfWork
    {
        IRepository<T> Repository<T>() where T : BaseEntity;
        bool SaveChanges();
        bool SaveChanges(string UID);
    }
}
