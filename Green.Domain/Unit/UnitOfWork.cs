﻿using Green.Domain.DB.Context;
using Green.Domain.DB.Context.Abstraction;
using Green.Domain.DB.Repository.Abstraction;
using Green.Domain.Entity.Abstraction;
using Green.Domain.Unit.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Unit
{
    public class UnitOfWork : IUnitOfWork
    {
        public UnitOfWork(IGreenDbContext greenDbContext)
        {
            GreenDbContext = greenDbContext;
        }

        private IGreenDbContext GreenDbContext { get; set; }

        public IRepository<T> Repository<T>() where T : BaseEntity
        {
            return GreenDbContext.GetRepository<T>();
        }

        public bool SaveChanges()
        {
            return GreenDbContext.SaveChanges();
        }

        public bool SaveChanges(string UID)
        {
            return GreenDbContext.SaveChanges(UID);
        }
    }
}
