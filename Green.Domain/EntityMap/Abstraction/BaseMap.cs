﻿using Green.Domain.Entity.Abstraction;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.EntityMap.Abstraction
{
    public abstract class BaseMap<T> : IEntityTypeConfiguration<T> where T : BaseEntity
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.Property(x => x.Id)
                   .HasColumnName("ID")
                   .UseIdentityColumn();

            builder.Property(x => x.CreationDate)
                   .HasColumnName("SYS_CREATION_DATE");

            builder.Property(x => x.CreationOwner)
                   .HasColumnName("SYS_CREATION_UID");

            builder.Property(x => x.EditDate)
                   .HasColumnName("SYS_EDIT_DATE");

            builder.Property(x => x.EditOwner)
                   .HasColumnName("SYS_EDIT_UID");

            builder.Property(x => x.Delete)
                   .HasColumnName("SYS_DELETED");
        }
    }
}
