﻿using Green.Domain.Entity;
using Green.Domain.EntityMap.Abstraction;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.EntityMap
{
    public class DiscountMap : BaseMap<DiscountDM>
    {
        public override void Configure(EntityTypeBuilder<DiscountDM> builder)
        {
            builder.ToTable("DISCOUNTS");

            builder.Property(x => x.Code)
                   .HasColumnName("CODE");

            builder.Property(x => x.IsActive)
                   .HasColumnName("ACTIVE");

            builder.Property(x => x.DiscountPercent)
                   .HasColumnName("DISCOUNT_PERCENT");

            builder.Property(x => x.DiscountAmount)
                   .HasColumnName("DISCOUNT_AMOUNT");

            builder.Property(x => x.Type)
                   .HasColumnName("TYPE");

            base.Configure(builder);
        }
    }
}
