﻿using Green.Domain.Entity;
using Green.Domain.EntityMap.Abstraction;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.EntityMap
{
    public class PackageMap : BaseMap<PackageDM>
    {
        public override void Configure(EntityTypeBuilder<PackageDM> builder)
        {
            builder.Property(x => x.Header)
                   .HasColumnName("PACKAGE_HEADER");

            builder.Property(x => x.Description)
                   .HasColumnName("PACKAGE_BODY");

            builder.Property(x => x.Price)
                   .HasColumnName("PRICE");

            builder.Property(x => x.PerDistance)
                   .HasColumnName("PER_DISTANCE");

            builder.Property(x => x.PerTime)
                   .HasColumnName("PER_TIME");

            builder.Property(x => x.Type)
                   .HasColumnName("TYPE");

            builder.ToTable("PACKAGES");

            base.Configure(builder);
        }
    }
}
