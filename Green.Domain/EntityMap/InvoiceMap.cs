﻿using Green.Domain.Entity;
using Green.Domain.EntityMap.Abstraction;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.EntityMap
{
    public class InvoiceMap : BaseMap<InvoiceDM>
    {
        public override void Configure(EntityTypeBuilder<InvoiceDM> builder)
        {
            builder.ToTable("INVOICES");

            builder.Property(x => x.InvoiceNumber)
                   .HasColumnName("INVOICE_NUMBER");

            builder.Property(x => x.UserClientId)
                   .HasColumnName("USER_CLIENT_ID");

            builder.Property(x => x.BikeId)
                   .HasColumnName("BIKE_ID");

            builder.Property(x => x.PackageId)
                   .HasColumnName("RIDE_PACKAGE_ID");

            builder.Property(x => x.DiscountId)
                   .HasColumnName("DISCOUNT_PACKAGE_ID");

            builder.Property(x => x.TotalAmount)
                   .HasColumnName("TOTAL_AMOUNT");

            builder.Property(x => x.Distance)
                   .HasColumnName("DISTANCE");

            builder.Property(x => x.Time)
                   .HasColumnName("TIME");

            builder.Property(x => x.Rate)
                   .HasColumnName("RATE");



            builder.HasOne(x => x.UserClient)
                   .WithMany()
                   .HasForeignKey(x => x.UserClientId);

            builder.HasOne(x => x.Discount)
                   .WithMany()
                   .HasForeignKey(x => x.DiscountId);

            builder.HasOne(x => x.Package)
                   .WithMany()
                   .HasForeignKey(x => x.PackageId);

            builder.HasOne(x => x.Bike)
                   .WithMany()
                   .HasForeignKey(x => x.BikeId);

            base.Configure(builder);
        }
    }
}
