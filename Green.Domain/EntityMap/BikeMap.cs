﻿using Green.Domain.Entity;
using Green.Domain.EntityMap.Abstraction;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.EntityMap
{
    public class BikeMap : BaseMap<BikeDM>
    {
        public override void Configure(EntityTypeBuilder<BikeDM> builder)
        {
            builder.ToTable("BIKE");

            builder.Property(x => x.BikeUID)
                   .HasColumnName("BIKE_UID");
            builder.Property(x => x.Plate)
                   .HasColumnName("PLATE");
            builder.Property(x => x.Charge)
                   .HasColumnName("CHARGE");
            builder.Property(x => x.Fee)
                   .HasColumnName("FEE");
            builder.Property(x => x.Lock)
                   .HasColumnName("LOCK");
            base.Configure(builder);

        }
    }
}
