﻿using Green.Domain.Entity;
using Green.Domain.EntityMap.Abstraction;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.EntityMap
{
    public class InvoiceImageMap : BaseMap<InvoiceImageDM>
    {
        public override void Configure(EntityTypeBuilder<InvoiceImageDM> builder)
        {
            builder.ToTable("INVOICE_IMAGES");

            builder.Property(x => x.InvoiceId)
                   .HasColumnName("INVOICE_ID");

            builder.Property(x => x.Image)
                   .HasColumnName("IMAGE");

            base.Configure(builder);
        }
    }
}
