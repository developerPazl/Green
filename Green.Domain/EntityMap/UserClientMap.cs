﻿using Green.Domain.Entity;
using Green.Domain.EntityMap.Abstraction;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.EntityMap
{
    public class UserClientMap : BaseMap<UserClientDM>
    {
        public override void Configure(EntityTypeBuilder<UserClientDM> builder)
        {
            builder.ToTable("USER_CLIENT");

            builder.Property(x => x.Name)
                   .HasColumnName("NAME");

            builder.Property(x => x.Surname)
                   .HasColumnName("SURNAME");

            builder.Property(x => x.Username)
                   .HasColumnName("USERNAME");

            builder.Property(x => x.Password)
                   .HasColumnName("PASSWORD_HASH");

            builder.Property(x => x.PhoneNumber)
                   .HasColumnName("PHONE_NUMBER");

            builder.Property(x => x.EMail)
                   .HasColumnName("EMAIL");

            base.Configure(builder);
        }
    }
}
