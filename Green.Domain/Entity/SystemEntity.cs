﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Entity
{
    public class SystemEntity
    {
        public DateTime? EditDate { get; internal set; } //Nullable
        public DateTime CreationDate { get; internal set; }
        public string EditOwner { get; internal set; } //Nullable
        public string CreationOwner { get; internal set; }
        public bool Delete { get; internal set; }
    }
}
