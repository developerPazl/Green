﻿using Green.Domain.Entity.Abstraction;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Entity
{
    public class InvoiceDM  : BaseEntity
    {
        public string InvoiceNumber { get; set; }
        public int UserClientId { get; set; }
        public int BikeId { get; set; }
        public int PackageId { get; set; }
        public int? DiscountId { get; set; }
        public decimal? TotalAmount { get; set; }
        public int? Distance { get; set; }
        public int? Time { get; set; }
        public int? Rate { get; set; }


        public UserClientDM UserClient { get; set; }
        public BikeDM Bike { get; set; }
        public PackageDM Package { get; set; }
        public DiscountDM Discount { get; set; }
    }
}
