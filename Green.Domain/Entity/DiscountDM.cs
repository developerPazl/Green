﻿using Green.Domain.Entity.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Entity
{
    public class DiscountDM : BaseEntity
    {
        public string Code { get; set; }
        public bool IsActive { get; set; }
        public int DiscountPercent { get; set; }
        public decimal DiscountAmount { get; set; }
        public int Type { get; set; }
    }
}
