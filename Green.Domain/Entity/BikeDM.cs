﻿using Green.Domain.Entity.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Entity
{
    public class BikeDM : BaseEntity
    {
        public string BikeUID { get; set; }
        public string Plate { get; set; }
        public int Charge { get; set; }
        public decimal Fee { get; set; }
        public bool Lock { get; set; }
    }
}
