﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Entity.Abstraction
{
    public abstract class BaseEntity : IEntity
    {
        public int Id { get; set; }
        public SystemEntity System 
        { 
            get
            {
                return new SystemEntity()
                {
                    EditDate = EditDate,
                    EditOwner = EditOwner,
                    CreationDate = CreationDate,
                    CreationOwner = CreationOwner,
                    Delete = Delete
                };
            }
        }



        internal DateTime? EditDate { get; set; } //Nullable
        internal DateTime CreationDate { get; set; } = DateTime.Now;
        internal string EditOwner { get; set; } //Nullable
        internal string CreationOwner { get; set; }
        internal bool Delete { get; set; }

    }
}
