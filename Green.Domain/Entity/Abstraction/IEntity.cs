﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Entity.Abstraction
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
