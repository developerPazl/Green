﻿using Green.Domain.Entity.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Entity
{
    public class InvoiceImageDM : BaseEntity
    {
        public int InvoiceId { get; set; }
        public string Image { get; set; }
    }
}
