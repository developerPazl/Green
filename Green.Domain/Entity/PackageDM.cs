﻿using Green.Domain.Entity.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Entity
{
    public class PackageDM : BaseEntity
    {
        public string Header { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int PerDistance { get; set; }
        public int PerTime { get; set; }
        public int Type { get; set; }
    }
}
