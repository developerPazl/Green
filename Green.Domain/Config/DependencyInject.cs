﻿using Green.Common.Config.Absraction;
using Green.Domain.DB.Context;
using Green.Domain.DB.Context.Abstraction;
using Green.Domain.Unit.Abstraction;
using Green.Domain.Unit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.Config
{
    public class DependencyInject : IDependencyInject
    {
        public IDependencyInject Register(IServiceCollection services = null, IConfiguration configuration = null)
        {
            services.AddScoped<IGreenDbContext, GreenDbContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddDbContext<GreenDbContext>(options =>
                             options.UseSqlServer(configuration.GetConnectionString("GreenDBContext")));

            return this;
        }
    }
}
