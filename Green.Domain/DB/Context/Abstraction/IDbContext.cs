﻿using Green.Domain.DB.Repository.Abstraction;
using Green.Domain.Entity.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.DB.Context.Abstraction
{
    public interface IDbContext 
    {
        public IRepository<T> GetRepository<T>() where T : BaseEntity;
        public bool SaveChanges();
        public bool SaveChanges(string UID);
    }
}
