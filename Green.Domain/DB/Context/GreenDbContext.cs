﻿using Green.Domain.EntityMap;
using Microsoft.EntityFrameworkCore;
using System;
using Green.Domain.Entity.Abstraction;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Green.Domain.DB.Context.Abstraction;
using Green.Domain.DB.Repository.Abstraction;
using Green.Domain.DB.Repository;

namespace Green.Domain.DB.Context
{
    public class GreenDbContext : DbContext, IGreenDbContext
    {
        public GreenDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UserClientMap());
            modelBuilder.ApplyConfiguration(new BikeMap());
            modelBuilder.ApplyConfiguration(new DiscountMap());
            modelBuilder.ApplyConfiguration(new PackageMap());
            modelBuilder.ApplyConfiguration(new InvoiceMap());
            modelBuilder.ApplyConfiguration(new InvoiceImageMap());
        }
        public bool SaveChanges(string UID)
        {
            EntitySetting(UID);
            try
            {
                this.SaveChanges();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        void EntitySetting(string UID)
        {

            var AddEdEntity = this.ChangeTracker.Entries().Where(x => x.State == EntityState.Added).ToList();
            var ModifiedEntity = this.ChangeTracker.Entries().Where(x => x.State == EntityState.Modified).ToList();

            try
            {
                this.ChangeTracker.AutoDetectChangesEnabled = false;

                foreach (var Entry in AddEdEntity)
                {
                    var BaseEntity = Entry.Entity as BaseEntity;

                    if (BaseEntity == null || BaseEntity.System == null)
                        continue;


                    BaseEntity.CreationDate = DateTime.Now;
                    BaseEntity.CreationOwner = UID;
                }

                foreach (var Entry in ModifiedEntity)
                {

                    var BaseEntity = Entry.Entity as BaseEntity;

                    if (BaseEntity == null || BaseEntity.System == null)
                        continue;

                    this.Entry(BaseEntity).Property(x => x.CreationDate).IsModified = false;
                    this.Entry(BaseEntity).Property(x => x.CreationOwner).IsModified = false;
                    this.Entry(BaseEntity).Property(x => x.EditDate).IsModified = true;
                    this.Entry(BaseEntity).Property(x => x.EditOwner).IsModified = true;

                    BaseEntity.System.EditDate = DateTime.Now;
                    BaseEntity.System.EditOwner = UID;
                }
            }
            finally
            {
                this.ChangeTracker.AutoDetectChangesEnabled = true;
            }
        }

        public IRepository<T> GetRepository<T>() where T : BaseEntity
        {
            var repository = new Repository<T>(this);
            return repository;
        }

        public bool SaveChanges()
        {
            return this.SaveChanges("SYSTEM");
        }
    }
}
