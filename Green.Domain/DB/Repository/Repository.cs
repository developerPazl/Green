﻿using Green.Domain.DB.Repository.Abstraction;
using Green.Domain.Entity.Abstraction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.DB.Repository
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        public Repository(DbContext context)
        {
            _context = context;
            dbSet = context.Set<T>();
        }

        private DbContext _context { get; set; }
        private DbSet<T> dbSet { get; set; }

        public void Add(T Entity)
        {
            dbSet.Add(Entity);
        }

        public void AddList(List<T> Entities)
        {
            dbSet.AddRange(Entities.ToArray());
        }

        public bool Any(Expression<Func<T, bool>> Predicate)
        {
            return dbSet.Any(Predicate);
        }

        public void Delete(T Entity)
        {
            Entity.System.Delete = true;
            Update(Entity);
        }

        public List<T> Find(Expression<Func<T, bool>> Predicate)
        {
            return dbSet.Where(Predicate).ToList();
        }

        public T Get(Expression<Func<T, bool>> Predicate)
        {
            return dbSet.FirstOrDefault(Predicate);
        }

        public void Update(T Entity)
        {
            _context.Entry(Entity).State = EntityState.Modified;
        }
        public void Update(T entity, params Expression<Func<T, object>>[] properties)
        {
            var entry = _context.Entry(entity);
            _context.Set<T>().Attach(entity);
            foreach (var property in properties)
                entry.Property(property).IsModified = true;
        }
    }
}
