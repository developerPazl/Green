﻿using Green.Domain.Entity.Abstraction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Green.Domain.DB.Repository.Abstraction
{
    public interface IRepository<T> where T : BaseEntity
    {
        void Add(T Entity);
        void AddList(List<T> Entities);
        void Update(T Entity);
        void Update(T Entity, params Expression<Func<T, object>>[] UpdatedProperties);
        T Get(Expression<Func<T, bool>> Predicate);
        bool Any(Expression<Func<T, bool>> Predicate);
        List<T> Find(Expression<Func<T, bool>> Predicate);
        void Delete(T Entity);
    }
}
