﻿using Green.Common.Model.Const;
using Green.Common.Model.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Green.Extensions
{
    public static class StringExtensions
    {
        public static bool IsValidEmail(this string sender)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(sender);
                return addr.Address == sender;
            }
            catch
            {
                return false;
            }
        }
        public static bool IsValidPhoneNumber(this string sender)
        {
            if (sender == null)
            {
                return false;
            }
            if (Regex.Match(sender, CountryRegex.TurkeyPattern).Success)
            {
                return true;
            }
            return false;
        }
        public static Country GetCountry(this string sender)
        {
            if (Regex.Match(sender, CountryRegex.TurkeyPattern).Success)
            {
                return Country.Turkey;
            }
            return Country.Undefined;
        }
        public static bool IsValidPassword(this string sender)
        {
            if (sender == null)
            {
                return false;
            }
            bool isCaseSuccess = false;
            bool isSymbolSuccess = false;
            bool isNumberSuccess = false;
            for (char i = (char)65; i <= 90; i++)
            {
                if (sender.Contains((char)i))
                {
                    isCaseSuccess = true;
                    break;
                }
            }
            if( sender.Contains('.') || 
                sender.Contains(',') || 
                sender.Contains('/'))
            {
                isSymbolSuccess = true;
            }
            for (char i = (char)48; i <= 57; i++)
            {
                if (sender.Contains((char)i))
                {
                    isNumberSuccess = true;
                    break;
                }
            }

            if(isCaseSuccess && isNumberSuccess && isSymbolSuccess)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        public static bool IsValidBase64(this string sender)
        {
            Span<byte> buffer = new Span<byte>(new byte[sender.Length]);
            return Convert.TryFromBase64String(sender, buffer, out int bytesParsed);
        }
    }
}
