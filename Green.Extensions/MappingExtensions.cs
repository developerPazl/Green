﻿using AutoMapper;
using Green.Common.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Green.Extensions
{
    public static class MappingExtensions
    { 
        public static T Map<T>(this object sender)
        {
            return GMapperConfiguration.GetMapper.Map<T>(sender);
        }
    }
}
